Mojave Wallpaper Generator
==========================
Compatible with macOS 10.14 Betas 1-4 **Untested on beta 5**

![App Icon](Assets/iconWeb.png)

- [Mojave Wallpaper Generator](#mojave-wallpaper-generator)
    - [1. Introduction](#1-introduction)
    - [2. Using Mojave Wallpaper Generator](#2-using-mojave-wallpaper-generator)
        - [2.1. Download prebuilt binary](#21-download-prebuilt-binary)
        - [2.2. Build from source](#22-build-from-source)
    - [3. FAQ](#3-faq)
        - [3.1. What is altitude and azimuth? How am I supposed to know what to set them to?](#31-what-is-altitude-and-azimuth-how-am-i-supposed-to-know-what-to-set-them-to)
        - [3.2. Your codebase is crap and you should feel bad.](#32-your-codebase-is-crap-and-you-should-feel-bad)
        - [3.3. Why is the dark/light checkbox grayed out?](#33-why-is-the-darklight-checkbox-grayed-out)
    - [4. Licence](#4-licence)
    - [5. Credits](#5-credits)

## 1. Introduction

Mojave Wallpaper Generator is a program which allows users to create their own dynamic wallpapers for use with macOS 10.14 Mojave. 

## 2. Using Mojave Wallpaper Generator

There are two ways to use Mojave Wallpaper Generator:
- Download a prebuilt binary (recommended)
- Build from source (requires Xcode 10)

### 2.1. Download prebuilt binary
Simply download the latest .app from [tags](https://gitlab.com/chase9/mojave-wallpaper-generator/tags) and start making wallpapers!
### 2.2. Build from source
1. Clone mojave-wallpaper-generator to your machine:
    ```` bash
    cd ~/Documents
    git clone https://gitlab.com/chase9/mojave-wallpaper-generator.git 
    ````
2. Build/compile the project:
   ```` bash
   cd mojave-wallpaper-generator
   xcodebuild
   ````
3. Run Mojave Wallpaper Generator:
   ```` bash
   open build/Release/Mojave\ Wallpaper\ Generator.app
   ````

## 3. FAQ
### 3.1. What is altitude and azimuth? How am I supposed to know what to set them to?
This is described in the application by clicking on the help button shown here:
![Help Button](Assets/helpButton.png)
However, the short answer is to use [this site](https://keisan.casio.com/exec/system/1224682277) to calculate altitude and azimuth for a location and time.
### 3.2. Your codebase is crap and you should feel bad.
You're probably right! I built this as a way to learn macOS programming and expect I've made a number of mistakes. If you want to help me out make a contribution or contact me (constructively!) so I can learn to be a better programmer ;)
### 3.3. Why is the dark/light checkbox grayed out?
Sometime around 10.14 beta 4 Apple changed its layout of the metadata the OS uses to display dynamic wallpapers. Unfortunately it looks like they removed this option, but the checkbox remains in case they decide to re-add it.

## 4. Licence
Mojave Wallpaper Generator is distributed under the MIT licence.

## 5. Credits
A huge thank you to Marcin Czachurski, whos research into Mojave's dynamic wallpapers inspired and enabled me to build this application. Checkout [his CLI version of this application](https://github.com/mczachurski/wallpapper), as well as [his Medium post](https://itnext.io/macos-mojave-dynamic-wallpaper-fd26b0698223)!