//
//  Document.swift
//  Mojave Wallpaper Generator
//
//  Created by Chase Lau on 7/5/18.
//  Copyright © 2018 Chase Lau. All rights reserved.
//

import Cocoa
import os.log
import AVFoundation

class Document: NSDocument {

  override init() {
    super.init()
    // Add your subclass-specific initialization here.
  }

  var wallpaper = DynamicWallpaper()
  var primaryWindowController: NSWindowController?
  var primaryViewController: ViewController?

  override class var autosavesInPlace: Bool {
    return false
  }

  override func makeWindowControllers() {
    // Returns the Storyboard that contains your Document window.
    let storyboard = NSStoryboard(name: NSStoryboard.Name("Main"), bundle: nil)
    let windowController = storyboard.instantiateController(withIdentifier: NSStoryboard.SceneIdentifier("Document Window Controller")) as! NSWindowController
    primaryWindowController = windowController
    primaryViewController = primaryWindowController?.contentViewController as? ViewController
    self.addWindowController(windowController)
    self.setContent()
  }

  override func data(ofType typeName: String) throws -> Data {
    // Insert code here to write your document to data of the specified type. If outError != nil, ensure that you create and set an appropriate error when returning nil.
    // You can also choose to override fileWrapperOfType:error:, writeToURL:ofType:error:, or writeToURL:ofType:forSaveOperation:originalContentsURL:error: instead.

    throw NSError(domain: NSOSStatusErrorDomain, code: unimpErr, userInfo: nil)
  }

  override func read(from data: Data, ofType typeName: String) throws {
    // Disable light mode switch due to incompatability with v2
    primaryViewController?.lightModeButton.isEnabled = false

    let options = [kCGImageSourceTypeIdentifierHint: AVFileType.heic as CFString]

    if let wallpaperSource = CGImageSourceCreateWithData(data as CFData, options as CFDictionary) {
      let wallpaper = DynamicWallpaper()

      for index in 0..<CGImageSourceGetCount(wallpaperSource) {
        if let image = CGImageSourceCreateImageAtIndex(wallpaperSource, index, nil) {
          // FIXME: Reading existing information
          wallpaper.addSlide(withImage: image, altitude: 0.0, onlyDisplayInDarkMode: false, azimuth: 0.0)
        }
      }

      self.wallpaper = wallpaper

      // Now we need to decode the image metadata into base64...
      if let metadata = CGImageSourceCopyMetadataAtIndex(wallpaperSource, 0, nil) {
        let metadataTag = CGImageMetadataCopyTagWithPath(metadata, nil, "apple_desktop:solar" as CFString)
        let base64 = CGImageMetadataTagCopyValue(metadataTag!) as! String
        if let decodedData = Data(base64Encoded: base64, options: .ignoreUnknownCharacters) {

          // Then we need to convert the base64 data into a property list...
          var plistFormat = PropertyListSerialization.PropertyListFormat.binary
          do {
            let decodedPlist = try PropertyListDecoder().decode(dynamicStructv2_2.self, from: decodedData, format: &plistFormat)
            // And read the information into our application!
            for slide in decodedPlist.keyGroup {
              self.wallpaper.slides![slide.i].altitude = NSNumber(value: slide.a)
              self.wallpaper.slides![slide.i].azimuth = NSNumber(value: slide.z)
            }

          } catch {
            // Catch for v2.2 Failure
            do {
              // Try again with v2.1
              let decodedPlist = try PropertyListDecoder().decode(dynamicStructv2_1.self, from: decodedData, format: &plistFormat)
              for slide in decodedPlist.keyGroup2 {
                self.wallpaper.slides![slide.i].altitude = NSNumber(value: slide.a)
                self.wallpaper.slides![slide.i].azimuth = NSNumber(value: slide.z)
              }
            } catch {
              // Catch for v2.1 Failure
              do {
                // Try again with v1
                let decodedPlist = try PropertyListDecoder().decode(dynamicStructv1.self, from: decodedData, format: &plistFormat)
                // Reenable light mode switch if we're using v1
                primaryViewController?.lightModeButton.isEnabled = true
                for slide in decodedPlist.keyGroup {
                  self.wallpaper.slides![slide.i].altitude = NSNumber(value: slide.a)
                  self.wallpaper.slides![slide.i].azimuth = NSNumber(value: slide.z)
                }
              } catch {
                // Catch for v1 Failure
                let alert = NSAlert(error: error)
                alert.runModal()
              }
              // v1 Catch
            }
            // v2.1 Catch
          }
          // v2.2 Catch
        }
      }

    } else {
      throw NSError(domain: NSOSStatusErrorDomain, code: unimpErr, userInfo: ["KEY": "Unable to create image from file"])
    }
  }

  override func save(withDelegate delegate: Any?, didSave didSaveSelector: Selector?, contextInfo: UnsafeMutableRawPointer?) {
    // First we need to get the slides we've been building.
    guard let viewController = primaryWindowController?.contentViewController as? ViewController else {
      fatalError("Unable to instantiate main View Controller")
    }
    let wallpaper = viewController.wallpaper

    // Now we ask where the user wants to save the new wallpaper...
    let panel = NSSavePanel()
    panel.allowedFileTypes = ["heic"]
    panel.runModal()

    // And save it!
    if let dir = panel.url {
      let progressBar = primaryViewController?.progressBar
      progressBar?.doubleValue = 0.0
      progressBar?.startAnimation(nil)
      progressBar?.isIndeterminate = true

      DispatchQueue.global().async {
        let destinationData = NSMutableData()
        if let destination = CGImageDestinationCreateWithData(destinationData, AVFileType.heic as CFString, wallpaper.slides.count, nil) {
          let options = [kCGImageDestinationLossyCompressionQuality: 1.0]

          let wallpaperCount = wallpaper.slides.count
          DispatchQueue.main.async {
            progressBar?.maxValue = Double(wallpaperCount) + 2.0
            progressBar?.isIndeterminate = false
          }

          defer {
            DispatchQueue.main.async {
              progressBar?.stopAnimation(nil)
              progressBar?.isHidden = true
            }
          }

          for index in 0..<wallpaperCount {
            let image = wallpaper.slides[index].image
            if index == 0 {
              let imageMetadata = CGImageMetadataCreateMutable()

              guard CGImageMetadataRegisterNamespaceForPrefix(imageMetadata, "http://ns.apple.com/namespace/1.0/" as CFString, "apple_desktop" as CFString, nil) else {
                fatalError("Namespace not registered.")
              }

              let sequenceInfo = self.createPropertyList(from: wallpaper)
              var base64PropertyList = ""
              do {
                base64PropertyList = try self.createBase64PropertyList(sequenceInfo: sequenceInfo)
              } catch {
                let alert = NSAlert(error: error)
                alert.runModal()
                break
              }

              let imageMetadataTag = CGImageMetadataTagCreate("http://ns.apple.com/namespace/1.0/" as CFString, "apple_desktop" as CFString, "solar" as CFString, CGImageMetadataType.string, base64PropertyList as CFTypeRef)

              guard CGImageMetadataSetTagWithPath(imageMetadata, nil, "apple_desktop:solar" as CFString, imageMetadataTag!) else {
                fatalError("Unable to add image tag.")
              }

              CGImageDestinationAddImageAndMetadata(destination, image, imageMetadata, options as CFDictionary)
            } else {
              CGImageDestinationAddImage(destination, image, options as CFDictionary)
            }
            DispatchQueue.main.async {
              progressBar?.increment(by: 1.0)
            }
          }

          CGImageDestinationFinalize(destination)
          DispatchQueue.main.async {
            progressBar?.increment(by: 1.0)
          }
          let imageData = destinationData as Data
          let fileURL = dir

          do {
            try imageData.write(to: fileURL)
            DispatchQueue.main.async {
              progressBar?.increment(by: 1.0)
            }
          } catch {
            let alert = NSAlert(error: error)
            alert.runModal()
          }
        }
      }
    } // if let dir = panel.url
  }

  private func setContent() {
    primaryViewController?.setWallpaper(to: wallpaper)
  }

  private func createPropertyList(from wallpaper: DynamicWallpaper) -> SequenceInfo {

    let sequenceInfo = SequenceInfo()

    for index in 0..<wallpaper.slides.count {
      let sequenceItem = SequenceItem()
      sequenceItem.a = Double(truncating: wallpaper.slides[index].altitude)
      sequenceItem.z = Double(truncating: wallpaper.slides[index].azimuth)
      sequenceItem.o = wallpaper.slides[index].onlyDisplayInLightMode == true ? 1 : 0
      sequenceItem.i = index

      sequenceInfo.si.append(sequenceItem)
    }

    return sequenceInfo
  }

  private func createBase64PropertyList(sequenceInfo: SequenceInfo) throws -> String {

    let encoder = PropertyListEncoder()
    encoder.outputFormat = .binary
    let plistData = try encoder.encode(sequenceInfo)

    let base64PropertyList = plistData.base64EncodedString()
    return base64PropertyList
  }

}

struct dynamicStructv1: Decodable {
  struct slideData: Decodable {
    var o: Int
    var i: Int
    var z: Double
    var a: Double
  }

  var keyGroup: [slideData]

  enum CodingKeys: String, CodingKey {
    case keyGroup = "si"
  }

}

struct dynamicStructv2_1: Decodable {
  struct wallpaperInfo: Decodable {
    var d: Int
    var l: Int
  }

  struct slideData: Decodable {
    var a: Double
    var i: Int
    var z: Double
  }

  var keyGroup1: wallpaperInfo
  var keyGroup2: [slideData]

  enum CodingKeys: String, CodingKey {
    case keyGroup1 = "ap"
    case keyGroup2 = "si"
  }

}

struct dynamicStructv2_2: Decodable {

  struct slideData: Decodable {
    var a: Double
    var i: Int
    var z: Double
  }

  var keyGroup: [slideData]

  enum CodingKeys: String, CodingKey {
    case keyGroup = "si"
  }

}
