//
//  DynamicWallpaper.swift
//  Mojave Wallpaper Generator
//
//  Created by Chase Lau on 7/5/18.
//  Copyright © 2018 Chase Lau. All rights reserved.
//

import Cocoa
import os.log

struct Slide {
  var image: CGImage
  var altitude: NSNumber
  var onlyDisplayInLightMode: Bool
  var azimuth: NSNumber
}

class DynamicWallpaper {
  var slides: [Slide]!

  init() {
    slides = [Slide]()
  }

  func addSlide(withImage image: CGImage, altitude: NSNumber, onlyDisplayInDarkMode: Bool, azimuth: NSNumber) {
    slides.append(Slide(image: image, altitude: altitude, onlyDisplayInLightMode: onlyDisplayInDarkMode, azimuth: azimuth))
  }

  func removeSlide(_ atIndex: Int) {
    slides.remove(at: atIndex)
  }
}
