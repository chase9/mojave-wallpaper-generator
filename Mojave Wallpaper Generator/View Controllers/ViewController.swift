//
//  ViewController.swift
//  Mojave Wallpaper Generator
//
//  Created by Chase Lau on 7/5/18.
//  Copyright © 2018 Chase Lau. All rights reserved.
//

import Cocoa
import os.log

class ViewController: NSViewController {

  // MARK: - Properties
  var wallpaper = DynamicWallpaper()

  // MARK: Outlets
  @IBOutlet weak var tableView: NSTableView!
  @IBOutlet weak var imageView: NSImageView!
  @IBOutlet weak var altitudeField: NSTextField!
  @IBOutlet weak var azimuthField: NSTextField!
  @IBOutlet weak var lightModeButton: NSButton!
  @IBOutlet weak var progressBar: NSProgressIndicator!
  
  // MARK: Actions
  @IBAction func lightModeButtonClicked(_ sender: NSButtonCell) {
    guard let selectedRow = tableView?.selectedRow else {
      fatalError("Unable to find tableview")
    }
    
    if selectedRow > -1 {
      var slide = wallpaper.slides[selectedRow]
      
      slide.onlyDisplayInLightMode = sender.state == .on ? true : false
    }
  }
  
  @IBAction func addSlide(_ sender: NSButton) {
    if let window = view.window {
      let panel = NSOpenPanel()
      panel.canChooseFiles = true
      panel.canChooseDirectories = false
      panel.allowsMultipleSelection = false
      panel.beginSheetModal(for: window) { (result) in
        if result == NSApplication.ModalResponse.OK {
          guard let url = panel.urls.first else {
            let userInfo = [NSLocalizedFailureErrorKey: "Unable to load image URL"]
            let error = NSError(domain: NSURLErrorDomain, code: -1, userInfo: userInfo)
            NSAlert(error: error).runModal()
            return
          }

          if let imageSource = CGImageSourceCreateWithURL(url as CFURL, nil) {
            guard let image = CGImageSourceCreateImageAtIndex(imageSource, 0, nil) else {
              let userInfo = [NSLocalizedFailureErrorKey: "Unable to load image"]
              let error = NSError(domain: NSCocoaErrorDomain, code: -1, userInfo: userInfo)
              NSAlert(error: error).runModal()
              return
            }

            self.wallpaper.addSlide(withImage: image, altitude: 0.0, onlyDisplayInDarkMode: false, azimuth: 0.0)

            DispatchQueue.main.async {
              print("Added slide: \(self.wallpaper.slides.count)")
              self.tableView.reloadData()
            }
          }
        }
      }
    }
  } // addSlide

  @IBAction func delete(_ sender: AnyObject) {
    let selectedRows = self.tableView.selectedRowIndexes

    if selectedRows.count != 0 {
      for index in selectedRows {
        self.wallpaper.removeSlide(index)
      }
      self.tableView.removeRows(at: selectedRows, withAnimation: .effectFade)
    }
  } // delete

  // MARK: - View Controller
  override func viewDidLoad() {
    tableView.delegate = self
    tableView.dataSource = self
    altitudeField.delegate = self
    azimuthField.delegate = self
    
    super.viewDidLoad()
  }

  override var representedObject: Any? {
    didSet {
      // Update the view, if already loaded.
    }
  }

  func updateView() {
    guard let selectedRow = tableView?.selectedRow else {
      fatalError("Unable to find tableview")
    }

    if selectedRow >= 0 {
      let slide = wallpaper.slides[selectedRow]

      imageView.image = NSImage(cgImage: slide.image, size: NSSize(width: slide.image.width, height: slide.image.height))
      altitudeField.stringValue = "\(slide.altitude)"
      azimuthField.stringValue = "\(slide.azimuth)"
      lightModeButton.state = slide.onlyDisplayInLightMode == true ? .on : .off
    }
  }
  
  func setWallpaper(to wallpaper: DynamicWallpaper) {
    self.wallpaper = wallpaper
    self.tableView.reloadData()
  }
}

// MARK: - Table View Delegate

extension ViewController: NSTableViewDelegate {

  fileprivate enum CellIdentifiers {
    static let SlideCell = "SlideCellID"
  }

  func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
    var image: CGImage
    let text: String = "\(row)"
    let cellIdentifier: String = CellIdentifiers.SlideCell

    let slide = wallpaper.slides[row]
    image = slide.image

    if let cell = tableView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: cellIdentifier), owner: nil) as? NSTableCellView {
      cell.textField?.stringValue = text
      cell.imageView?.image = NSImage(cgImage: image, size: NSSize(width: image.width, height: image.height))
      return cell
    }
    return nil
  }

  func tableViewSelectionDidChange(_ notification: Notification) {
    updateView()
  }

} // NSTableViewDelegate

// MARK: - Table View Data Source

extension ViewController: NSTableViewDataSource {

  func numberOfRows(in tableView: NSTableView) -> Int {
    return wallpaper.slides.count
  }
}

// MARK: - Text Delegate
extension ViewController: NSTextFieldDelegate {

  override func controlTextDidEndEditing(_ obj: Notification) {

    guard let sender = obj.object as? NSTextField else {
      fatalError("Unknown Sender: \(String(describing: obj.object))")
    }

    guard let identifier = sender.identifier else {
      fatalError("No identifier for sender: \(String(describing: sender))")
    }

    // Proper number formatting
    let formatter = NumberFormatter()
    formatter.numberStyle = .decimal

    switch identifier.rawValue {
    case "altitude":
      formatter.maximum = 90
      formatter.minimum = -90
    case "azimuth":
      formatter.maximum = 360
      formatter.minimum = 0
    default:
      fatalError("1Unknown identifier: \(identifier.rawValue)")
    }

    let newNumber = formatter.number(from: sender.stringValue) ?? 0.0
    sender.stringValue = formatter.string(from: newNumber) ?? "0.0"

    // Save slide data
    if tableView.selectedRow > -1 {

      switch identifier.rawValue {
      case "altitude":
        wallpaper.slides[tableView.selectedRow].altitude = newNumber
      case "azimuth":
        wallpaper.slides[tableView.selectedRow].azimuth = newNumber
      default:
        fatalError("2Unknown identifier: \(identifier.rawValue)")
      }
    }
  }

} // NSTextFieldDelegate
