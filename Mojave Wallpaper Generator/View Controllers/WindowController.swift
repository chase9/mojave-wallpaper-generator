//
//  WindowController.swift
//  Mojave Wallpaper Generator
//
//  Created by Chase Lau on 7/29/18.
//  Copyright © 2018 Chase Lau. All rights reserved.
//

import Cocoa

class WindowController: NSWindowController {

  override func windowDidLoad() {
    super.windowDidLoad()

    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
  }

  required init?(coder: NSCoder) {
    super.init(coder: coder)
    shouldCascadeWindows = true
  }

}
