//
//  HelpViewController.swift
//  Mojave Wallpaper Generator
//
//  Created by Chase Lau on 7/6/18.
//  Copyright © 2018 Chase Lau. All rights reserved.
//

import Cocoa

class HelpViewController: NSViewController {

  // MARK: Actions
  
  @IBAction func learnMoreButtonPushed(_ sender: Any) {
    if let url = URL(string: "https://itnext.io/macos-mojave-dynamic-wallpaper-fd26b0698223") {
      NSWorkspace.shared.open(url)
    }
  }
  
  @IBAction func conversionButtonPushed(_ sender: Any) {
    if let url = URL(string: "https://keisan.casio.com/exec/system/1224682277") {
      NSWorkspace.shared.open(url)
    }
  }
  
  @IBAction func imageSource(_ sender: Any) {
    if let url = URL(string: "https://en.wikipedia.org/wiki/Horizontal_coordinate_system") {
      NSWorkspace.shared.open(url)
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do view setup here.
  }

}
